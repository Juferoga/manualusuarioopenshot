#  📖 ManualUsuarioOpenshot

## Realizado por : 👨 Kevin Holguin @Kevin-HB18

Proyecto gnubies 2019 que generara un manual de usuario para el editor de video Openshot

## installacion y puesta en marcha del proyecto

Pra ver la documentacion **no traducida** hay que instalar la herramienta de
Sphinx docs la cual instalaremos mediante pip

### Para instalar PIP

Ejecutamos En terminal el comando dependiendo la version de python que tengamos

    apt install python-pip	#python 2
    apt install python3-pip	#python 3

### Para instalar SPHINX DOCS

Ejecutamos En terminal el comando

    pip install -U Sphinx
    
[Tutorial](https://www.youtube.com/watch?v=oJsUvBQyHBs)

# Para Latex

## Herramientas utilizadas

laTeX, Overleaf

## Notas Del Proyecto

....


